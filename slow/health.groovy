import me.tom.forge.groovymacros.OverlayRenderer
import me.tom.forge.groovymacros.math.vector.vec4.impl.Color
import me.tom.forge.groovymacros.wrapper.HUDElement
import org.lwjgl.opengl.GL11

long start = System.currentTimeMillis()
double time = 0

/*--------------<SETTINGS>--------------*/
final Color HEALTH_COLOR = Color.RED
final Color POISON_COLOUR = Color.GREEN
final Color WITHER_COLOUR = Color.DARK_GREY
final Color HEALTH_DECREASE_COLOR = Color.DARK_RED
final Color HEALTH_INCREASE_COLOR = new Color(1, 0.3f, 0.3f)

final double SCALE = 1
final int BAR_STYLE = 8
/*--------------</SETTINGS>--------------*/

float lastHealth = me.health / me.maxHealth

disableHUDElement(HUDElement.HEALTH)
overlay("statusBar", { OverlayRenderer renderer, float partialTicks ->
	int gamemode = me.gameMode
	if (gamemode != 1 && gamemode != 3)
	{
		long now = System.currentTimeMillis()
		double delta = (now - start) / 1000.0
		time += delta
		start = now

		int barWidth = 128 * SCALE
		int barHeight = 15 * SCALE
		double center = (renderer.width / 2) - (barWidth) / 2
		double barY = (renderer.height / 2)

		float currentHealth = me.health / me.maxHealth
		lastHealth += ((currentHealth - lastHealth) * delta)

		Color changeColor = currentHealth > lastHealth ? HEALTH_INCREASE_COLOR : HEALTH_DECREASE_COLOR
		Color mainColor = HEALTH_COLOR
		if (me.hasPotionEffect("minecraft:wither"))
			mainColor = WITHER_COLOUR
		else if (me.hasPotionEffect("minecraft:poison"))
			mainColor = POISON_COLOUR
		renderBar(renderer, center - 82, barY + 7.5, SCALE, BAR_STYLE, "${String.format("%.1f / %.1f", me.health, me.maxHealth)}", [changeColor, mainColor], [Math.max(lastHealth, currentHealth), Math.min(lastHealth, currentHealth)])
	}
})

private static void renderBar(OverlayRenderer renderer, double x, double y, double scale, int style, String text, List<Color> colors, List<Double> values)
{
	GL11.glColor4f(1, 1, 1, 0.33f)
	x /= scale
	y /= scale
	GL11.glPushMatrix()
	GL11.glScalef((float) scale, (float) scale, 1)
	renderer.texturedRectangle(x, y, 128, 16, "groovymacros:textures/bars.png", 0, style * 16, 256, 256)

	for (int i = 0; i < values.size(); i++)
	{
		Color color = colors[i]
		double value = values[i]

		GL11.glColor4f(color.r, color.g, color.b, color.a)
		renderer.texturedRectangle(x, y, value * 128, 16, "groovymacros:textures/bars.png", 0, (style * 16) + 16, 256, 256)
	}

	int width = renderer.getTextWidth(text)
	int middle = 63 - width / 2
	renderer.textWithShadow(text, x + middle * 2, y + (renderer.textHeight / 2), 1, 0xFFFFFF)
	GL11.glPopMatrix()
}
