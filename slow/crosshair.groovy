import me.tom.forge.groovymacros.OverlayRenderer
import me.tom.forge.groovymacros.math.vector.vec4.impl.Color
import me.tom.forge.groovymacros.wrapper.ItemStack
import org.lwjgl.input.Keyboard

/*--------------<SETTINGS>--------------*/
final double SCALE = 0.5
/*--------------</SETTINGS>--------------*/

disableHUDElement(HUDElement.CROSSHAIR)

overlay("crosshair", { OverlayRenderer renderer, float partialTicks ->

	int centerX = renderer.width
	int centerY = renderer.height
	org.lwjgl.opengl.GL11.glScalef(0.5f, 0.5f, 1)
	renderer.texturedRectangle(centerX-8.5, centerY-8.5, 17, 17, "groovymacros:textures/crosshair.png", 0, 0, 17, 17)
	org.lwjgl.opengl.GL11.glScalef(2f, 2f, 1)
})