import me.tom.forge.groovymacros.OverlayRenderer
import me.tom.forge.groovymacros.math.vector.vec4.impl.Color
import me.tom.forge.groovymacros.wrapper.ItemStack
import org.lwjgl.input.Keyboard

/*--------------<SETTINGS>--------------*/
final double SCALE = 1
final boolean WARN_ALMOST_BROKEN = true
final double WARNING_SPEED = 1
final double DURABILITY_THRESHOLD = 0.9
/*--------------</SETTINGS>--------------*/

boolean enabled = true
keyPressListener("armorHudToggle", Keyboard.KEY_H, { -> enabled = !enabled })


me.sendMessage("ArmorHud initialized.")
long start = System.currentTimeMillis()
float time = 0
overlay("test", { OverlayRenderer renderer, float partialTicks ->
	if(!enabled)
		return
	double centerX = (renderer.width/2)
	double centerY = (renderer.height/2)
	long now = System.currentTimeMillis()
	time += (now - start) / 1000.0
	start = now

	def color = Color.fromHSB(time * WARNING_SPEED as float, 1, 1)
	for (int i = 0; i < 4; i++)
	{
		ItemStack item = me.getArmor(3 - i)
		if (WARN_ALMOST_BROKEN && item.canBeDamaged() && item.metadata / item.maxDamage > DURABILITY_THRESHOLD)
			renderer.textWithShadow("*", centerX + 6 + (i * 16 * SCALE)+(162/2), centerY+47, SCALE, color)
		renderer.item(centerX + (i * 16 * SCALE)+(162/2), centerY+31, SCALE, item, true)
	}

//	renderer.text("Health: $me.health", 0, 200, 1, 0xFFFFFF)
//	renderer.text("Food: $me.foodLevel", 0, 200+renderer.textHeight, 1, 0xFFFFFF)
//	renderer.text("Saturation: $me.saturation", 0, 200+renderer.textHeight*2, 1, 0xFFFFFF)
})