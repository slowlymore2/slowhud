import me.tom.forge.groovymacros.OverlayRenderer
import me.tom.forge.groovymacros.math.vector.vec4.impl.Color
import me.tom.forge.groovymacros.wrapper.HUDElement
import me.tom.forge.groovymacros.wrapper.ItemStack
import org.lwjgl.opengl.GL11

/*--------------<SETTINGS>--------------*/
final double SCALE = 1
/*--------------</SETTINGS>--------------*/

final Color hotbarOpacity = new Color(0, 0, 0, 0.5f)
final Color selectedColour = new Color(1, 1, 1, 0.5f)
disableHUDElement(HUDElement.HOTBAR)

me.sendMessage("Hotbar Enabled")
overlay("hotbar", { OverlayRenderer renderer, float partialTicks ->
	int slot = me.selectedHotbarSlot - 1
	int slotSize = 18 * SCALE

	int centerX = renderer.width/2
	int centerY = renderer.height/2

	renderer.rectangle(centerX - (5 * slotSize) + 9, centerY+31, slotSize*9, slotSize, hotbarOpacity)
	renderer.rectangle((slot * slotSize)+ slotSize + centerX - (5 * slotSize) + 9, centerY+31, slotSize, slotSize, selectedColour)
	for (int i = 0; i < 10; i++)
	{
		ItemStack item = i == 0 ? me.itemInOffHand : me.getItem(i-1)
		double x = (i * slotSize) + centerX - (5 * slotSize) + 10
		double y = centerY + 32
		renderer.item(x-slotSize, y, SCALE, item, true)
		renderItemText(renderer, x-slotSize, y, item, false)
	}
})

void renderItemText(OverlayRenderer renderer, double x, double y, ItemStack item, boolean percent)
{
	GL11.glDisable(GL11.GL_DEPTH_TEST)
	double textY = y
	if(item.canBeDamaged())
	{
		int healthLeft = item.maxDamage - item.metadata
		renderer.textWithShadow(percent ? String.format("%.1f%%", (healthLeft / item.maxDamage) * 100) : healthLeft as String, x, textY, 0.5, 0xFFFFFF)
		textY += renderer.textHeight/2
	}

	int count = -item.count
	def targetType = item.type
	int targetMetadata = item.metadata
	for(int i=0;i<36;i++)
	{
		def loopItem = me.getItem(i)
		if(loopItem.type == targetType && (item.canBeDamaged() || loopItem.metadata == targetMetadata))
			count += loopItem.count
	}

	if(count > 0)
		renderer.textWithShadow("+$count", x, textY, 0.5, 0xFFFFFF)
	GL11.glEnable(GL11.GL_DEPTH_TEST)
}