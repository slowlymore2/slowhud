import me.tom.forge.groovymacros.OverlayRenderer
import me.tom.forge.groovymacros.math.vector.vec4.impl.Color
import me.tom.forge.groovymacros.wrapper.HUDElement

long start = System.currentTimeMillis()
double time = 0

/*--------------<SETTINGS>--------------*/
final Color XP_COLOR = new Color(0.3f, 1, 0.3f)
final Color XP_COLOR2 = new Color(0.2f, 0.6f, 0.2f)
final Color TEXT_COLOR = Color.LIME_GREEN
final Color TEXT_COLOR2 = new Color(0, 0.2f, 0)
/*--------------</SETTINGS>--------------*/

float lastExp = me.expLevelProgress
disableHUDElement(HUDElement.EXP)

overlay("slowXP", { OverlayRenderer renderer, float partialTicks ->
	long now = System.currentTimeMillis()
	double delta = (now - start) / 1000.0
	time += delta
	start = now

	int barWidth = 18 * 9
	int barHeight = 1
	double barX = (renderer.width/2)-(barWidth)/2
	double barY = (renderer.height/2)+49

	float currentExp = me.expLevelProgress
	lastExp += ((currentExp - lastExp) * delta * 3)
	renderer.rectangle(barX, barY, lastExp*barWidth, barHeight, XP_COLOR)
	renderer.rectangle(barX, barY+1, lastExp*barWidth, barHeight, XP_COLOR2)
	
	if(me.experienceLevel != 0)
	{
		String text = me.experienceLevel as String
		int textX = barX + (barWidth/2 - renderer.getTextWidth(text)/2)
		int textY = barY + 4

		renderer.text(text, textX+1, textY, 1, TEXT_COLOR2)
		renderer.text(text, textX, textY+1, 1, TEXT_COLOR2)
		renderer.text(text, textX-1, textY, 1, TEXT_COLOR2)
		renderer.text(text, textX, textY-1, 1, TEXT_COLOR2)
		renderer.text(text, textX, textY, 1, TEXT_COLOR)
	}
})