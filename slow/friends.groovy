import me.tom.forge.groovymacros.OverlayRenderer
import me.tom.forge.groovymacros.math.vector.vec4.impl.Color
import me.tom.forge.groovymacros.wrapper.Entity
import me.tom.forge.groovymacros.wrapper.Player
import org.lwjgl.input.Keyboard
import org.lwjgl.opengl.GL11

/*--------------<SETTINGS>--------------*/
final Color HEALTH_COLOR = Color.RED
final Color POISON_COLOUR = Color.GREEN
final Color WITHER_COLOUR = Color.DARK_GREY
final Color HEALTH_DECREASE_COLOR = Color.DARK_RED
final Color HEALTH_INCREASE_COLOR = new Color(1, 0.3f, 0.3f)

final double SCALE = 1
final int BAR_STYLE = 8
/*--------------</SETTINGS>--------------*/

File friendsFile = new File("./friends.txt")

List<UUID> friendIds = friendsFile.exists() && friendsFile.length() != 0 ? friendsFile.text.split("\n").collect(UUID.&fromString) : []
Closure isFriend = {Player p -> friendIds.contains(p.id)}
keyPressListener("addFriend", Keyboard.KEY_U, { ->
	Entity targetEntity = me.targetEntity
	if(targetEntity instanceof Player && targetEntity != me)
	{
		if(isFriend(targetEntity))
		{
			friendIds.remove(targetEntity.id)
			me.sendMessage("\u00a7aRemoved friend \u00a77$targetEntity.name")
			me.playSound("minecraft:entity.blaze.death", me.eyePosition, 0.5f, 0.5f)
		}else{
			friendIds.add(targetEntity.id)
			me.sendMessage("\u00a7aAdded friend \u00a77$targetEntity.name")
			me.playSound("minecraft:entity.player.levelup", me.eyePosition, 0.5f, 0.5f)
		}

		friendsFile.text = friendIds.join("\n")
	}
})

Map<UUID, Double> lastHealths = friendIds.collectEntries {[it: 1]}
long start = System.currentTimeMillis()
double time = 0

overlay("friends", {OverlayRenderer renderer, float partialTicks ->
	long now = System.currentTimeMillis()
	double delta = (now - start) / 1000.0
	time += delta
	start = now
	world.players.findAll(isFriend).eachWithIndex { Player friend, int i ->
		double currentHealth = friend.health / friend.maxHealth
		double lastHealth = lastHealths[friend.id] ?: 1
		lastHealth += ((currentHealth - lastHealth) * delta)
		lastHealths[friend.id] = lastHealth

		Color changeColor = currentHealth > lastHealth ? HEALTH_INCREASE_COLOR : HEALTH_DECREASE_COLOR
		Color mainColor = HEALTH_COLOR
		if(friend.hasPotionEffect("minecraft:wither"))
			mainColor = WITHER_COLOUR
		else if(friend.hasPotionEffect("minecraft:poison"))
			mainColor = POISON_COLOUR
		renderBar(renderer, 0, i*16, SCALE, BAR_STYLE, friend.name, String.format("%.1f%%", lastHealth*100), [changeColor, mainColor], [Math.max(lastHealth, currentHealth), Math.min(lastHealth, currentHealth)])
	}
})

private static void renderBar(OverlayRenderer renderer, double x, double y, double scale, int style, String textLeft, String textRight, List<Color> colors, List<Double> values)
{
	GL11.glColor4f(1, 1, 1, 0.33f)
	x /= scale
	y /= scale
	GL11.glPushMatrix()
	GL11.glScalef((float)scale, (float)scale, 1)
	renderer.texturedRectangle(x, y, 128, 16, "groovymacros:textures/bars.png", 0, style*16, 256, 256)

	for (int i = 0; i < values.size(); i++)
	{
		Color color = colors[i]
		double value = values[i]

		GL11.glColor4f(color.r, color.g, color.b, color.a)
		renderer.texturedRectangle(x, y, value * 128, 16, "groovymacros:textures/bars.png", 0, (style*16)+16, 256, 256)
	}

	renderer.textWithShadow(textLeft, x+4, y+ renderer.textHeight/2, 1, 0xFFFFFF)
	renderer.textWithShadow(textRight, (x+128)-renderer.getTextWidth(textRight), y+renderer.textHeight/2, 1, 0xFFFFFF)

//	int width = renderer.getTextWidth(text)
//	int middle = 0
//	renderer.textWithShadow(text, x + middle, y + (renderer.textHeight / 2), 1, 0xFFFFFF)
	GL11.glPopMatrix()
}