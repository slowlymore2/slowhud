import me.tom.forge.groovymacros.OverlayRenderer
import me.tom.forge.groovymacros.math.vector.vec4.impl.Color
import me.tom.forge.groovymacros.wrapper.HUDElement
import me.tom.forge.groovymacros.wrapper.ItemStack
import me.tom.forge.groovymacros.wrapper.Player
import org.lwjgl.opengl.GL11

long start = System.currentTimeMillis()
double time = 0

/*--------------<SETTINGS>--------------*/

final Color FOOD_COLOR = Color.ORANGE

final Color BREATH_COLOR = Color.SPARSE_BLUE
final Color BREATH_DECREASE_COLOR = BREATH_COLOR
final Color BREATH_INCREASE_COLOR = Color.BABY_BLUE

final double SCALE = 1
final int BAR_STYLE = 8
/*--------------</SETTINGS>--------------*/

float lastFoodLevel = me.foodLevel / 20.0
float lastBreath = me.remainingAir / 300.0
long notNow = 0

disableHUDElement(HUDElement.FOOD)
disableHUDElement(HUDElement.ARMOR)
disableHUDElement(HUDElement.AIR)

overlay("otherBars", { OverlayRenderer renderer, float partialTicks ->
	int gamemode = me.gameMode
	if (gamemode != 1 && gamemode != 3)
	{
		long now = System.currentTimeMillis()
		double delta = (now - start) / 1000.0
		time += delta
		start = now

		int barWidth = 128 * SCALE
		int barHeight = 15 * SCALE

		double center = (renderer.width / 2) - (barWidth) / 2
		double barY = (renderer.height / 2)

		float foodLevel = Math.max(0, Math.min(20, me.foodLevel))/20
		float foodTextLevel = foodLevel*20
		lastFoodLevel += ((foodLevel - lastFoodLevel) * delta)
		renderBar(renderer, center + 80, barY + 7.5, SCALE, BAR_STYLE, "${String.format("%.1f / %.1f", foodTextLevel, 20.0f)}", [FOOD_COLOR], [lastFoodLevel])

		float currentBreath = (me.remainingAir+20) / 320
		lastBreath += ((currentBreath - lastBreath) * delta)
		Color changeColourBreath = currentBreath > lastBreath ? BREATH_INCREASE_COLOR : BREATH_DECREASE_COLOR
		Color mainColourBreath = BREATH_COLOR
		if (world.getBlock(me.eyePosition).contains("water"))
			renderBar(renderer, center + 80, barY - 2.5, SCALE, BAR_STYLE, "${getBreathTimeString(me)}", [changeColourBreath, mainColourBreath], [Math.max(lastBreath, currentBreath), Math.min(lastBreath, currentBreath)])
	}
})

private static int getRespiration(Player self)
{
	ItemStack helmet = self.getArmor(3)
	if(helmet == null || helmet.isEmpty())
		return 0

	return helmet.enchantments."minecraft:respiration" ?: 0
}

private static String getBreathTimeString(Player self)
{
	double potionSeconds = (self.potionEffects.find {it.type == "minecraft:water_breathing"}?.duration ?: 0) / 20.0

	int respiration = getRespiration(self)+1
	double seconds = (self.remainingAir+20) / 20
	if(respiration == 1)
		return String.format("%.1fs", seconds+potionSeconds)
	else
		return String.format("~%.0fs", seconds*respiration+potionSeconds)
}

private static void renderBar(OverlayRenderer renderer, double x, double y, double scale, int style, String text, List<Color> colors, List<Double> values)
{
	GL11.glColor4f(1, 1, 1, 0.33f)
	x /= scale
	y /= scale
	GL11.glPushMatrix()
	GL11.glScalef((float) scale, (float) scale, 1)
	renderer.texturedRectangle(x, y, 128, 16, "groovymacros:textures/bars.png", 0, style * 16, 256, 256)

	for (int i = 0; i < values.size(); i++)
	{
		Color color = colors[i]
		double value = values[i]

		GL11.glColor4f(color.r, color.g, color.b, color.a)
		renderer.texturedRectangle(x, y, value * 128, 16, "groovymacros:textures/bars.png", 0, (style * 16) + 16, 256, 256)
	}

	int width = renderer.getTextWidth(text)
	int middle = 64 - width / 2
	renderer.textWithShadow(text, x + 3, y + (renderer.textHeight / 2), 1, 0xFFFFFF)
	GL11.glPopMatrix()
}
